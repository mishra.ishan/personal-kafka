package Joins;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

public class JoinMainTest {

    private TopologyTestDriver topologyTestDriver;
    private StringSerializer stringSerializer = new StringSerializer();
    private ConsumerRecordFactory<String, String> recordFactory;

    @Before
    public void setup() {

        JoinMain joinMain = new JoinMain();

        Properties properties = new Properties();
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "join-example");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(ProducerConfig.LINGER_MS_CONFIG, "1");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        // leverage idempotent producer from Kafka 0.11 !
        properties.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true"); // ensure we don't push duplicates

        recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);
        topologyTestDriver = new TopologyTestDriver(joinMain.createTopology(), properties);
    }

    @After
    public void close() {
        topologyTestDriver.close();
    }

    private void insertUserRecord(String key, String value) {
        topologyTestDriver.pipeInput(recordFactory.create("user-table", key, value));
    }

    private void insertPurchaseRecord(String key, String value) {
        topologyTestDriver.pipeInput(recordFactory.create("user-purchases", key, value));
    }

    private ProducerRecord<String,String> readOutputInnerJoin() {
        return topologyTestDriver.readOutput("inner-join-output", new StringDeserializer(), new StringDeserializer());
    }

    private ProducerRecord<String,String> readOutputLeftJoin() {
        return topologyTestDriver.readOutput("outer-join-output", new StringDeserializer(), new StringDeserializer());
    }

    @Test
    public void singleUserInnerJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertUserRecord("john", userInfo);
        insertPurchaseRecord("john", purchaseInfo);
//        insertUserRecord("john", userInfo);
        OutputVerifier.compareKeyValue(readOutputInnerJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=[" + userInfo + "]");
    }

    @Test
    public void singleUserPurchaseFirstInnerJoinTest() throws InterruptedException {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertPurchaseRecord("john", purchaseInfo);
        insertUserRecord("john", userInfo);
        //It is because when the 1st purchase comes, there is no user offset yet committed.
        Assert.assertEquals(readOutputInnerJoin(), null);
    }

    @Test
    public void noUserInnerJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        insertPurchaseRecord("katie", purchaseInfo);
        Assert.assertEquals(readOutputInnerJoin(), null);
    }

    @Test
    public void updateUserInnerJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertUserRecord("john", userInfo);
        insertPurchaseRecord("john", purchaseInfo);
        String updatedUserInfo = "First=Johnny,Last=Doe,Email=johnny.doe@gmail.com";
        String newPurchase = "Oranges (3)";
        insertUserRecord("john", updatedUserInfo);
        insertPurchaseRecord("john", newPurchase);
        OutputVerifier.compareKeyValue(readOutputInnerJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=[" + userInfo + "]");
        OutputVerifier.compareKeyValue(readOutputInnerJoin(), "john", "Purchase=" + newPurchase + ",UserInfo=[" + updatedUserInfo + "]");
        Assert.assertEquals(readOutputInnerJoin(), null);

    }

    @Test
    public void purchaseFirstUserLaterInnerJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String oldUserInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertPurchaseRecord("john", purchaseInfo);
        insertUserRecord("john", oldUserInfo);
        String updatedUserInfo = null;
        String newPurchase = "Oranges (3)";
        insertPurchaseRecord("john", newPurchase);
        insertUserRecord("john", updatedUserInfo);
        //When 1st purchase comes, that time there is no user. Hence, we do nothing (just the offset in purchase increases). When the second purchase comes, that time we have the user offset as the previous one.
        OutputVerifier.compareKeyValue(readOutputInnerJoin(), "john", "Purchase=" + newPurchase + ",UserInfo=[" + oldUserInfo + "]");
        Assert.assertEquals(readOutputInnerJoin(), null);
    }

    @Test
    public void userDeletedBeforePurchaseInnerJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String oldUserInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        String updatedUserInfo = null;
        insertUserRecord("john", oldUserInfo);
        insertUserRecord("john", updatedUserInfo);
        insertPurchaseRecord("john", purchaseInfo);
        //When the first purchase record comes, there is no user
        Assert.assertEquals(readOutputInnerJoin(), null);
    }

    @Test
    public void singleUserLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertUserRecord("john", userInfo);
        insertPurchaseRecord("john", purchaseInfo);
//        insertUserRecord("john", userInfo);
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=[" + userInfo + "]");
    }

    @Test
    public void singleUserPurchaseFirstLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
//        insertUserRecord("john", userInfo);
        insertPurchaseRecord("john", purchaseInfo);
        insertUserRecord("john", userInfo);
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=null");
    }

    @Test
    public void noUserLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        insertPurchaseRecord("katie", purchaseInfo);
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "katie", "Purchase=" + purchaseInfo + ",UserInfo=null");
    }

    @Test
    public void updateUserLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertUserRecord("john", userInfo);
        insertPurchaseRecord("john", purchaseInfo);
        String updatedUserInfo = "First=Johnny,Last=Doe,Email=johnny.doe@gmail.com";
        String newPurchase = "Oranges (3)";
        insertUserRecord("john", updatedUserInfo);
        insertPurchaseRecord("john", newPurchase);
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=[" + userInfo + "]");
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + newPurchase + ",UserInfo=[" + updatedUserInfo + "]");
//        OutputVerifier.compareKeyValue(readOutputInnerJoin(), "john", "Purchase=" + userInfo + ",UserInfo=[" + purchaseInfo + "]");
        Assert.assertEquals(readOutputLeftJoin(), null);
    }

    @Test
    public void purchaseFirstUserLaterLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        insertPurchaseRecord("john", purchaseInfo);
        insertUserRecord("john", userInfo);
        String updatedUserInfo = null;
        String newPurchase = "Oranges (3)";
        insertPurchaseRecord("john", newPurchase);
        insertUserRecord("john", updatedUserInfo);
        //When 1st purchase comes, there is no user.
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=null");
        //When 2nd purchase comes, the offset in userTable is at the first index.
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + newPurchase + ",UserInfo=[" + userInfo + "]");
        //There is no 3rd purchase.
        Assert.assertEquals(readOutputLeftJoin(), null);

    }

    @Test
    public void userDeletedBeforePurchaseLeftJoinTest() {
        String purchaseInfo = "Apples and Bananas (1)";
        String userInfo = "First=John,Last=Doe,Email=john.doe@gmail.com";
        String updatedUserInfo = null;
        insertUserRecord("john", userInfo);
        insertUserRecord("john", updatedUserInfo);
        insertPurchaseRecord("john", purchaseInfo);
        //When the 1ts purchase record comes, there is no user
        OutputVerifier.compareKeyValue(readOutputLeftJoin(), "john", "Purchase=" + purchaseInfo + ",UserInfo=null");
        Assert.assertEquals(readOutputInnerJoin(), null);
    }

    @Test
    public void test() {
        System.out.println(test.Test);
    }
}

enum test {
    Test("TEST");

    private String testStr;

    test(String test) {
        this.testStr = test;
    }
}