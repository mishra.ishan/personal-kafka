import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

public class FavouriteColourTest {
    private TopologyTestDriver topologyTestDriver;
    private ConsumerRecordFactory<String, String> recordFactory;
    private StringSerializer stringSerializer = new StringSerializer();

    @Before
    public void setUp() {
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "fav_color");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "store");
        properties.put(StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG, CustomRocksDBConfig.class);

        FavouriteColor favouriteColor = new FavouriteColor();
        recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);
        topologyTestDriver = new TopologyTestDriver(favouriteColor.createTopology(), properties);
    }

    @After
    public void closeTestDriver() {
        topologyTestDriver.close();
    }

    private void insertInput(String value) {
        topologyTestDriver.pipeInput(recordFactory.create("lucky-colour-input-topic", null, value));
    }

    private ProducerRecord<String, Long> readOutput() {
        return topologyTestDriver.readOutput("lucky-colour-output-topic", new StringDeserializer(), new LongDeserializer());
    }

    @Test
    public void favouriteValidColourSimpleTest() {
        String[] consumerRecords = {"ishan,blue", "heena,green", "aishwarya,red"};
        for(String record : consumerRecords) {
            insertInput(record);
        }
        OutputVerifier.compareKeyValue(readOutput(), "blue", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "green", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 1L);
        Assert.assertEquals(readOutput(), null);

    }

    @Test
    public void favouriteColourNewColourTest() {
        String[] consumerRecords = {"ishan,blue", "heena,green", "aishwarya,red", "ishan,red"};
        for(String record : consumerRecords) {
            insertInput(record);
        }
        OutputVerifier.compareKeyValue(readOutput(), "blue", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "green", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 1L);

        /**
         * Note that in favourite color application, in the end, we are converting the KTable output to a stream and writing to the output topic. That is, we shall first make a changelog topic of the KTable, and then dump that changelog to the output topic. This can be verified by this test. When we did (ishan,red), then the KTable first deleted (ishan,blue), and then updated the colour of ishan to be red. Thus, first it shall be (blue,0L), and then it shall be (red,2L).
         */
        OutputVerifier.compareKeyValue(readOutput(), "blue", 0L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 2L);
        Assert.assertEquals(readOutput(), null);
    }

    @Test
    public void favouriteColourUnknownColourTest() {
        String[] consumerRecords = {"ishan,blue", "heena,green", "aishwarya,red", "ishan,brown"};
        for(String record : consumerRecords) {
            insertInput(record);
        }
        OutputVerifier.compareKeyValue(readOutput(), "blue", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "green", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 1L);
        Assert.assertEquals(readOutput(), null);
    }

    @Test
    public void favouriteColourMultipleNewColourTest() {
        String[] consumerRecords = {"ishan,blue", "heena,green", "aishwarya,red", "ishan,red", "heena,blue", "aishwarya,blue"};
        for(String record : consumerRecords) {
            insertInput(record);
        }
        OutputVerifier.compareKeyValue(readOutput(), "blue", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "green", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 1L);

        /**
         * Note that in favourite color application, in the end, we are converting the KTable output to a stream and writing to the output topic. That is, we shall first make a changelog topic of the KTable, and then dump that changelog to the output topic. This can be verified by this test. When we did (ishan,red), then the KTable first deleted (ishan,blue), and then updated the colour of ishan to be red. Thus, first it shall be (blue,0L), and then it shall be (red,2L).
         *
         */
        OutputVerifier.compareKeyValue(readOutput(), "blue", 0L);
        OutputVerifier.compareKeyValue(readOutput(), "red", 2L);

        /**
         * * Similarly when we do (heena,blue), then first, we will have a subtractor changelog for original key,value pair (heena,green), and then an adder changelog for (heena,blue)
         */
        OutputVerifier.compareKeyValue(readOutput(), "green", 0L);
        OutputVerifier.compareKeyValue(readOutput(), "blue", 1L);

        OutputVerifier.compareKeyValue(readOutput(), "red", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "blue", 2L);
        Assert.assertEquals(readOutput(), null);
    }
}
