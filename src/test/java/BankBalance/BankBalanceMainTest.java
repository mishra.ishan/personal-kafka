package BankBalance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.Properties;

public class BankBalanceMainTest {

    private TopologyTestDriver topologyTestDriver;
    private ConsumerRecordFactory<String, String> recordFactory;
    private StringSerializer stringSerializer = new StringSerializer();
    private ObjectMapper objectMapper = new ObjectMapper();

    @Before
    public void setup() {

        BankBalanceMain bankBalanceMain = new BankBalanceMain();

        Properties properties = new Properties();
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "store");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "bank-balance");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(StreamsConfig.EXACTLY_ONCE, "true");

        //Disable the cache to demonstrate all the steps involved.
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        //Enable exactly once behaviour
        properties.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);

        recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);
        topologyTestDriver = new TopologyTestDriver(bankBalanceMain.createTopology(), properties);
    }

    @After
    public void closeTopology() {
        topologyTestDriver.close();
    }

    private void insertInput(Data data) throws JsonProcessingException {
        String value = objectMapper.writeValueAsString(data);
        System.out.println("------------- VALUE: " + value);
        topologyTestDriver.pipeInput(recordFactory.create("bank-balance-input-topic", data.getName(), value));
    }

    private ProducerRecord<String, BankBalanceAggregate> readOutput() {
        return topologyTestDriver.readOutput("bank-balance-output-topic", new StringDeserializer(), new BankBalanceAggregateDeserializer());
    }

    @Test
    public void sampleOnePersonTest() throws JsonProcessingException {
        long lastUpdatedAt0;
        long lastUpdatedAt1;
        long lastUpdatedAt2;
        long lastUpdatedAt3;
        Data[] dataArr = {
                new Data("Ishan", 1000.0, lastUpdatedAt0 =Instant.now().toEpochMilli()),
                new Data("Ishan", 2000.0, lastUpdatedAt1 = Instant.now().toEpochMilli()),
                new Data("Ishan", 3000.0, lastUpdatedAt2 = Instant.now().toEpochMilli()),
                new Data("Ishan", 4000.0, lastUpdatedAt3 = Instant.now().toEpochMilli()),
        };

        System.out.println(lastUpdatedAt0);
        System.out.println(lastUpdatedAt1);

        for(Data data : dataArr) {
            insertInput(data);
        }


        /**
         * Note how even the aggregation works on changelog mode. Everytime a stream of data comes in, there is a change in changelog. The table might just get updated, but the stream (being append only, and not update) would process all data points one by one only, and never as a batch.
         */
        OutputVerifier.compareKeyValue(readOutput(), "Ishan", new BankBalanceAggregate(1000d, lastUpdatedAt0));

        /**
         * Output expected When sent 2nd input
         */
        OutputVerifier.compareKeyValue(readOutput(), "Ishan", new BankBalanceAggregate(3000d, lastUpdatedAt1));

        /**
         * Output expected When sent 3rd input
         */
        OutputVerifier.compareKeyValue(readOutput(), "Ishan", new BankBalanceAggregate(6000d, lastUpdatedAt2));

        /**
         * Output expected When sent 4th input
         */
        OutputVerifier.compareKeyValue(readOutput(), "Ishan", new BankBalanceAggregate(10000d, lastUpdatedAt3));
        Assert.assertEquals(readOutput(), null);
    }
}
