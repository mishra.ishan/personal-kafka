import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.TopologyTestDriver;
import org.apache.kafka.streams.test.ConsumerRecordFactory;
import org.apache.kafka.streams.test.OutputVerifier;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Properties;

public class WordCountTest {

    private TopologyTestDriver topologyTestDriver;
    private ConsumerRecordFactory<String, String> recordFactory;
    private StringSerializer stringSerializer = new StringSerializer();


    /**
     * Create a method to initialise the topology test driver before any of the tests are run.
     */
    @Before
    public void setUp() {

        Properties properties = new Properties();
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "my-application-id-word-count");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        WordCountApplication wordCountApplication = new WordCountApplication();

        recordFactory = new ConsumerRecordFactory<>(stringSerializer, stringSerializer);

        topologyTestDriver = new TopologyTestDriver(wordCountApplication.createTopology(), properties);
    }

    /**
     * Similarly, create a method to close the topology test driver once the tests are run. If we don't close it, then if we run the tests again after the first time, then they will always fail. It is because topology test driver, if kept opened, it will assume the created state stores, are still there. Thus, when we run the test for 2-3 iterations, the new iteration WILL ALWAYS HAVE THE STATE (in this case, word counts) OF THE PREVIOUS ITERATIONS. Thus, when we try to compare the count, we would need to have the record of previous iteration count. For example, if we have run the test case 2-3 times, then the word "testing" in this case might have occurred 6-9 times respectively, and in our OutputKeyVerifier, we would have to make sure that we compare "testing" with 10L, and not 1 (for the 4th test iteration). Thus, it is advicable to always close the topology test driver after the tests.
     */
    @After
    public void closeTestDriver() {
        topologyTestDriver.close();
    }

    private void inputRecord(String value) {
        topologyTestDriver.pipeInput(recordFactory.create("word-count-input", null, value));
    }

    private ProducerRecord<String, Long> readOutput() {
        return topologyTestDriver.readOutput("word-count-output", new StringDeserializer(), new LongDeserializer());
    }

    @Test
    public void countsTest() {
        String firstInput = "testing kafka streams";
        inputRecord(firstInput);
        OutputVerifier.compareKeyValue(readOutput(), "testing", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "kafka", 1L);
        OutputVerifier.compareKeyValue(readOutput(), "streams", 1L);

        String secondInput = "testing kafka testing";
        inputRecord(secondInput);
        OutputVerifier.compareKeyValue(readOutput(), "testing", 2L);
        OutputVerifier.compareKeyValue(readOutput(), "kafka", 2L);
        OutputVerifier.compareKeyValue(readOutput(), "testing", 3L);
        Assert.assertEquals(readOutput(), null);
    }
}
