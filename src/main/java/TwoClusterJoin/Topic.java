package TwoClusterJoin;

public enum Topic {
    SOURCE ("DWH_SEARCH_EVENT"),
    TARGET ("DWH_SEARCH_EVENT");

    private String topicName;

    Topic(String topicName) {
        this.topicName = topicName;
    }

    public String getTopicName() {
        return topicName;
    }
}
