package TwoClusterJoin;

import TwoClusterJoin.Avro.SearchEvent;
import io.confluent.kafka.serializers.KafkaAvroDeserializer;
import io.confluent.kafka.serializers.KafkaAvroDeserializerConfig;
import io.confluent.kafka.serializers.KafkaAvroSerializer;
import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ProduceConsumerDifferentCluster {

    private Topology createTopology() {
        StreamsBuilder builder = new StreamsBuilder();

        builder.stream(Topic.SOURCE.getTopicName())
                .foreach((k,v) -> {
                    Properties properties = new Properties();
                    try (InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream("target_kafka.properties")) {
                        properties.load(resourceAsStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    System.out.println("---------------------> Kafka avro ser name:->" + KafkaAvroSerializer.class.getName());
                    properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class.getName());
                    final KafkaProducer producer = new KafkaProducer<String, SearchEvent>(properties);

                });

        return builder.build();
    }

    public static void main(String[] args) {
        Properties properties = new Properties();
        try(final InputStream ioStream = ProduceConsumerDifferentCluster.class.getClassLoader().getResourceAsStream("source_kafka.properties")) {
            properties.load(ioStream);
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        System.out.println("---------------------> Kafka avro deser name:->" + KafkaAvroDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaAvroDeserializer.class.getName());
        ProduceConsumerDifferentCluster pcdc = new ProduceConsumerDifferentCluster();
        final KafkaStreams streams = new KafkaStreams(pcdc.createTopology(), properties);
        streams.start();



    }
}
