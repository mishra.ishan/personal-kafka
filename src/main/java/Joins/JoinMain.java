package Joins;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.GlobalKTable;
import org.apache.kafka.streams.kstream.KStream;

import java.util.Properties;

public class
JoinMain {

    public Topology createTopology() {
        StreamsBuilder builder = new StreamsBuilder();
        GlobalKTable<Object, Object> userTable = builder.globalTable("user-table");
        KStream<Object, Object> userPurchases = builder.stream("user-purchases");

        /**
         * Suppose there is stream 'M' join stream 'N'. Then UNTIL THE RECORD FOR M (stream which HAS TRIGGERED THE JOIN) arrives, JOIN DOES NOT GETS TRIGGERED.
         * Join will get triggered once the record for 'M' arrives. That time, the join shall be triggered for the 'M' record join 'N' record.
         * If 'N' is a table, then the latest matching key shall be joined.
         * If 'N' is a stream, then since it is update only, thus the updates will have to wait to be committed.
         */
        //1. Inner join
        KStream<Object, String> innerJoin = userPurchases.join(userTable,
                (k, v) -> k, //select key from stream. Since we are joining with 1st param as stream, (k,v) are key value pairs of stream. We select the key for the join. This key shall be the same key as table.
                (streamVal, tableVal) -> "Purchase=" + streamVal + ",UserInfo=[" + tableVal + "]");

        innerJoin.to("inner-join-output");


        //2. Left join
        KStream<Object, String> leftJoin = userPurchases.leftJoin(userTable,
                (k, v) -> k, //select key from stream. Since we are joining with 1st param as stream, (k,v) are key value pairs of stream. We select the key for the join. This key shall be the same key as table.
                (streamVal, tableVal) -> {
                    if(tableVal == null) {
                        return "Purchase=" + streamVal + ",UserInfo=null";
                    }
                    else {
                        return "Purchase=" + streamVal + ",UserInfo=[" + tableVal + "]";
                    }
                });

        leftJoin.to("outer-join-output");

        return builder.build();
    }

    public static void main(String[] args) {



        JoinMain joinMain = new JoinMain();

        Properties properties = new Properties();
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "join-example");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(ProducerConfig.LINGER_MS_CONFIG, "1");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());


        KafkaStreams streams = new KafkaStreams(joinMain.createTopology(), properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}
