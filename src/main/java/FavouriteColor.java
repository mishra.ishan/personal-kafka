import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.config.TopicConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.*;
import org.apache.kafka.streams.errors.StreamsException;
import org.apache.kafka.streams.kstream.*;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.processor.ThreadMetadata;
import org.apache.kafka.streams.processor.TimestampExtractor;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.QueryableStoreType;
import org.apache.kafka.streams.state.RocksDBConfigSetter;
import org.rocksdb.BlockBasedTableConfig;
import org.rocksdb.Options;

import java.util.Map;
import java.util.Properties;

public class FavouriteColor {

    public Topology createTopology() {
        //Create topology
        StreamsBuilder builder = new StreamsBuilder();

        //Source the topic
        builder.stream("lucky-colour-input-topic")
                .filter((k,v) -> v.toString().split(",").length == 2)
                .filter((k,v) -> v.toString().split(",")[1].toLowerCase().equalsIgnoreCase("blue") ||
                                v.toString().split(",")[1].toLowerCase().equalsIgnoreCase("red") ||
                        v.toString().split(",")[1].toLowerCase().equalsIgnoreCase("green"))
                .peek((k,v) -> System.out.println("BEFORE MAP Key:" + k + ", Value:" + v))
                .map((k,v) -> {
                    String[] split = v.toString().split(",");
                    return new KeyValue<>(split[0], split[1]);
                })
                .peek((k,v) -> System.out.println("AFTER MAP Key:" + k + ", Value:" + v))
                .to("lucky-colour-intermediate-topic", Produced.with(Serdes.String(), Serdes.String()));

        /**
         * table:-
         * Create a {@link KTable} for the specified topic.
         * The {@code "auto.offset.reset"} strategy, {@link TimestampExtractor}, key and value deserializers
         * are defined by the options in {@link Consumed} are used.
         * Input {@link KeyValue records} with {@code null} key will be dropped.
         * <p>
         * Note that the specified input topic must be partitioned by key.
         * If this is not the case the returned {@link KTable} will be corrupted.
         * <p>
         * The resulting {@link KTable} will be materialized in a local {@link KeyValueStore} using the given
         * {@code Materialized} instance.
         * However, no internal changelog topic is created since the original input topic can be used for recovery (cf.
         * methods of {@link KGroupedStream} and {@link KGroupedTable} that return a {@link KTable}).
         * <p>
         * You should only specify serdes in the {@link Consumed} instance as these will also be used to overwrite the
         * serdes in {@link Materialized}, i.e.,
         * <pre> {@code
         * streamBuilder.table(topic, Consumed.with(Serde.String(), Serde.String(), Materialized.<String, String, KeyValueStore<Bytes, byte[]>as(storeName))
         * }
         * </pre>
         * To query the local {@link KeyValueStore} it must be obtained via
         * {@link KafkaStreams#store(String, QueryableStoreType) KafkaStreams#store(...)}:
         * <pre>{@code
         * KafkaStreams streams = ...
         * ReadOnlyKeyValueStore<String, Long> localStore = streams.store(queryableStoreName, QueryableStoreTypes.<String, Long>keyValueStore());
         * String key = "some-key";
         * Long valueForKey = localStore.get(key); // key must be local (application state is shared over all running Kafka Streams instances)
         * }</pre>
         * For non-local keys, a custom RPC mechanism must be implemented using {@link KafkaStreams#allMetadata()} to
         * query the value of the key on a parallel running instance of your Kafka Streams application.
         *
         * @param topic              the topic name; cannot be {@code null}
         * @param consumed           the instance of {@link Consumed} used to define optional parameters; cannot be {@code null}
         * @param materialized       the instance of {@link Materialized} used to materialize a state store; cannot be {@code null}
         * @return a {@link KTable} for the specified topic
         */
        KTable<String, Long> finalKTable = builder.table("lucky-colour-intermediate-topic", Consumed.with(Serdes.String(), Serdes.String()))
                .groupBy((k, v) -> new KeyValue<>(v, k))
                .count(Materialized.with(Serdes.String(), Serdes.Long()).as("Lucky-Colour-KTable"));

        finalKTable.toStream().to("lucky-colour-output-topic", Produced.with(Serdes.String(), Serdes.Long()));

        return builder.build();
    }

    public static void main(String[] args) {

        FavouriteColor favouriteColor = new FavouriteColor();

        //Define properties for the stream.
        Properties properties = new Properties();
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "fav_color");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "store");
        properties.put(StreamsConfig.ROCKSDB_CONFIG_SETTER_CLASS_CONFIG, CustomRocksDBConfig.class);

        KafkaStreams kafkaStreams = new KafkaStreams(favouriteColor.createTopology(), properties);

        //Stream shall be string string. Every time a new value comes into the stream, we need to make changes in the current counter.

        /**
         * Do a clean up of the local {@link StateStore} directory ({@link StreamsConfig#STATE_DIR_CONFIG}) by deleting all
         * data with regard to the {@link StreamsConfig#APPLICATION_ID_CONFIG application ID}.
         * <p>
         * May only be called either before this {@code KafkaStreams} instance is {@link #start() started} or after the
         * instance is {@link #close() closed}.
         * <p>
         * Calling this method triggers a restore of local {@link StateStore}s on the next {@link #start() application start}.
         *
         * @throws IllegalStateException if this {@code KafkaStreams} instance is currently {@link KafkaStreams.State#RUNNING running}
         * @throws StreamsException if cleanup failed
         */
        kafkaStreams.cleanUp();
        kafkaStreams.start();

        /**
         * getRuntime:-
         * Returns the runtime object associated with the current Java application.
         * Most of the methods of class <code>Runtime</code> are instance
         * methods and must be invoked with respect to the current runtime object.
         *
         * @return  the <code>Runtime</code> object associated with the current
         *          Java application.
         */
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));

        /**
         * LocalThreadsMetadata:-
         * Returns runtime information about the local threads of this {@link KafkaStreams} instance.
         *
         * @return the set of {@link ThreadMetadata}.
         */
        /**
         * ThreadMetadata:-
         * Represents the state of a single thread running within a {@link KafkaStreams} application.
         */
        while(true) {
            kafkaStreams.localThreadsMetadata().forEach(System.out::println);
            try {
                Thread.sleep(5000);
            }
            catch (InterruptedException ex) {
                System.err.println(ex);
//                ex.printStackTrace();
                break;
            }
        }
    }
}