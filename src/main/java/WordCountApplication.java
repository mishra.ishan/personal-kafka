import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;

import java.util.Arrays;
import java.util.Properties;

public class WordCountApplication {

    public Topology createTopology() {
        //Create topology
        StreamsBuilder builder = new StreamsBuilder();
        //Obtain the stream from topic (SOURCE)
        KStream<String, String> wordCountInputStream = builder.stream("word-count-input");

        KTable<String, Long> wordVsCountTable = wordCountInputStream
                .mapValues(value -> value.toLowerCase())
                .flatMapValues(value -> Arrays.asList(value.split(" ")))
                .selectKey((key, value) -> value)
                .groupByKey()
                /**
                 * Materialise the grouping into a table with a name 'CountsTable'. This table is called KTable.
                 * This is an aggregation and in every aggregation, the streams application create a table from stream.
                 */
                .count(Materialized.as("CountsTable"));

        //Dump back to topic (SINK)
        wordVsCountTable.toStream().to("word-count-output", Produced.with(Serdes.String(), Serdes.Long()));

        return builder.build();
    }

    public static void main(String[] args) {

        WordCountApplication wordCountApplication = new WordCountApplication();

        //Create properties
        Properties properties = new Properties();
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        /**
         * Application id is important in a streams application. This property is specific to streams application only.
         * 1. For any consumer in our streams application, the application id = consumer group id for that streams consumer.
         * 2. Also it is the default client id prefix.
         * 3. And it is also the prefix to the internal changelog topics that are created by streams internally.
         *
         * If we change the application id, then our streams application will think that it's a new kafka streams application and will start to reprocess the data.
         */
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "my-application-id-word-count");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());

        //We have given the logic of our stream, now we need to start the stream.
        KafkaStreams kafkaStreams = new KafkaStreams(wordCountApplication.createTopology(), properties);

        /**
         *  When we start a streams application, we generally have a restore consumer too, which tries to restore the last committed offsets.
         *  Also, the transition of stream states is ->  (create consumer clients and producers and restore consumers etc)CREATED -> REBALANCING / STARTING -> PARTITIONS_REVOKED -> PARTITIONS_ASSIGNED -> RUNNING
         *
         * In case of graceful shutdown:-
         *  RUNNING -> PENDING_SHUTDOWN -> DEAD.
         *  */
        kafkaStreams.start();

        /**
         * This is an interesting thing to note. We add shutdown hooks to any program (not just streams applications) in order to do a graceful shutdown.
         * When we do kill -15 <pid>, a SIGTERM command is issued by the kernel to the pid for which the command was issued (in our case, the streams application). SIGINT (interrupt signal)(kill -2) is issued if we close the application by a 'Ctr + C' command. Also a SIGTERM is issued to all running applications in case the machine shuts down. Whenever a SIGTERM command or a SIGINT is issued (or any such command which can be caught (see the quora link attached)), the shutdown hook works. It basically does a cleanup of our program before quitting the application.
         * When we do kill -9 <pid>, then a SIGKILL command is issued. NOTHING CAN BE DONE IF S SIGKILL COMMAND IS ISSUED. All we can do is that we can add another script watching the pid of that application in a polling fashion. When that script finds out that the pid is not present, it can run a new application which does the cleanup work.
         * https://stackoverflow.com/questions/2541597/how-to-gracefully-handle-the-sigkill-signal-in-java/2541618#2541618
         * https://www.quora.com/What-is-the-difference-between-the-SIGINT-and-SIGTERM-signals-in-Linux-What%E2%80%99s-the-difference-between-the-SIGKILL-and-SIGSTOP-signals
         *
         *                  ABOUT SIGNALS
         * SIGINT is the interrupt signal. The terminal sends it to the foreground process when the user presses ctrl-c. The default behavior is to terminate the process, but it can be caught or ignored. The intention is to provide a mechanism for an orderly, graceful shutdown.
         *
         * SIGQUIT is the dump core signal. The terminal sends it to the foreground process when the user presses ctrl-\. The default behavior is to terminate the process and dump core, but it can be caught or ignored. The intention is to provide a mechanism for the user to abort the process. You can look at SIGINT as "user-initiated happy termination" and SIGQUIT as "user-initiated unhappy termination."
         *
         * SIGTERM is the termination signal. The default behavior is to terminate the process, but it also can be caught or ignored. The intention is to kill the process, gracefully or not, but to first allow it a chance to cleanup.
         *
         * SIGKILL is the kill signal. The only behavior is to kill the process, immediately. As the process cannot catch the signal, it cannot cleanup, and thus this is a signal of last resort.
         *
         * SIGSTOP is the pause signal. The only behavior is to pause the process; the signal cannot be caught or ignored. The shell uses pausing (and its counterpart, resuming via SIGCONT) to implement job control.
         */
        Runtime.getRuntime().addShutdownHook(new Thread(kafkaStreams::close));

        // print the topology every 10 seconds for learning purposes
        while(true){
            kafkaStreams.localThreadsMetadata().forEach(data -> System.out.println(data));
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
