package BankBalance;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.Properties;

public class BankBalanceMain {

    public Topology createTopology() {
        StreamsBuilder builder = new StreamsBuilder();
        BankBalanceAggregateSerializer bankBalanceAggregateSerializaer = new BankBalanceAggregateSerializer();
        BankBalanceAggregateDeserializer bankBalanceAggregateDeserializaer = new BankBalanceAggregateDeserializer();

        String topic = "bank-balance-input-topic";

        //Had to do builder.<String,String>stream(topic) because otherwise, aggregate method does not know the types and it will try to infer it as <Object, Object>.
        builder.<String, String>stream(topic)
                .groupByKey()
                .aggregate(BankBalanceAggregate::empty,
                        BankBalanceAggregate::adder,
                        Materialized.<String, BankBalanceAggregate, KeyValueStore<Bytes, byte[]>>as("bank-balance-state-store")
                                .withKeySerde(Serdes.String())
                                .withValueSerde(Serdes.serdeFrom(bankBalanceAggregateSerializaer, bankBalanceAggregateDeserializaer)))
                .toStream().to("bank-balance-output-topic", Produced.with(Serdes.String(), Serdes.serdeFrom(bankBalanceAggregateSerializaer, bankBalanceAggregateDeserializaer)));

        return builder.build();
    }

    public static void main(String[] args) {

        BankBalanceMain bankBalanceMain = new BankBalanceMain();

        Properties properties = new Properties();
        properties.put(StreamsConfig.STATE_DIR_CONFIG, "store");
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "bank-balance");
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(StreamsConfig.EXACTLY_ONCE, "true");

        //Disable the cache to demonstrate all the steps involved.
        properties.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        //Enable exactly once behaviour
        properties.put(StreamsConfig.PROCESSING_GUARANTEE_CONFIG, StreamsConfig.EXACTLY_ONCE);

        KafkaStreams streams = new KafkaStreams(bankBalanceMain.createTopology(), properties);

        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}
