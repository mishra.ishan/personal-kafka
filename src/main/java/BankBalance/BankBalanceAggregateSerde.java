package BankBalance;

import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serializer;

public class BankBalanceAggregateSerde implements Serde<BankBalanceAggregate> {
    @Override
    public Serializer<BankBalanceAggregate> serializer() {
        return new BankBalanceAggregateSerializer();
    }

    @Override
    public Deserializer<BankBalanceAggregate> deserializer() {
        return new BankBalanceAggregateDeserializer();
    }
}
