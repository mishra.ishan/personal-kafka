package BankBalance;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Deserializer;

import java.io.IOException;
import java.util.Map;

public class BankBalanceAggregateDeserializer implements Deserializer<BankBalanceAggregate> {

    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void configure(Map<String, ?> configs, boolean isKey) {

    }

    @Override
    public BankBalanceAggregate deserialize(String topic, byte[] data) {
        if(data == null) {
            return BankBalanceAggregate.empty();
        }
        try {
            BankBalanceAggregate bankBalanceAggregate = objectMapper.readValue(data, BankBalanceAggregate.class);
            return bankBalanceAggregate;
        }
        catch (JsonParseException e) {
            e.printStackTrace();
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return BankBalanceAggregate.empty();
    }

    @Override
    public void close() {

    }
}
