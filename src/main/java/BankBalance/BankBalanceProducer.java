package BankBalance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.*;

public class BankBalanceProducer {

    private static Logger logger = LoggerFactory.getLogger(BankBalanceProducer.class);

    public static void main(String[] args) {

        Properties properties = new Properties();
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.put(ProducerConfig.ACKS_CONFIG, "all");
        //Will enable exactly once behaviour for the producer.
        properties.put(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        properties.put(ProducerConfig.RETRIES_CONFIG, "3");
        //Done so that the producer sends the records quickly (without buffering the records). Not to be done in production.
        properties.put(ProducerConfig.LINGER_MS_CONFIG, 1);

        KafkaProducer<String, String> kafkaProducer = new KafkaProducer<>(properties);

        ObjectMapper mapper = new ObjectMapper();
        Random random = new Random();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    generateAndSendRandom(kafkaProducer, mapper, random);
                } catch (JsonProcessingException e) {
                   logger.error("       ==================================================> Exception Occurred: ", e);
                }
            }
        }, 0, 1000);
    }

    public static void generateAndSendRandom(KafkaProducer<String, String> kafkaProducer, ObjectMapper mapper, Random random) throws JsonProcessingException {

        List<String> nameList = new ArrayList<>(Arrays.asList("Ishan", "Heena", "Aishwarya", "Shivam", "Jojo", "Barbie"));

        String topic = "bank-balance-input-topic";

        Collections.shuffle(nameList);

        for(int i = 0, ind = 0; i < 100; i++, ind++) {

            if(ind == nameList.size()) {
                ind = 0;
                Collections.shuffle(nameList);
            }

            String name = nameList.get(ind);
            Double amount = random.nextInt(999999) * random.nextDouble();

            Data data = new Data(name, amount, Instant.now().toEpochMilli());

            ProducerRecord<String, String> record = new ProducerRecord<>(topic, name, mapper.writeValueAsString(data));

            kafkaProducer.send(record, (recordMetadata, e) -> {
                // executes every time a record is successfully sent or an exception is thrown
                if (e == null) {
                    // the record was successfully sent
                    logger.info("Received new metadata. \n" +
                            "Topic:" + recordMetadata.topic() + "\n" +
                            "Partition: " + recordMetadata.partition() + "\n" +
                            "Offset: " + recordMetadata.offset() + "\n" +
                            "Timestamp: " + recordMetadata.timestamp());
                } else {
                    logger.error("Error while producing", e);
                }
            });

        }
    }
}
