package BankBalance;

public class Data {
    String name;
    Double money;
    long updatedTime;

    public Data() {
    }

    public Data(String name, Double money, long updatedTime) {
        this.name = name;
        this.money = money;
        this.updatedTime = updatedTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public long getUpdatedTime() {
        return updatedTime;
    }

    public void setUpdatedTime(long updatedTime) {
        this.updatedTime = updatedTime;
    }
}
