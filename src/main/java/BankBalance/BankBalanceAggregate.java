package BankBalance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Objects;

public class BankBalanceAggregate {
    private Double aggregateAmount;
    private long lastUpdatedAt;

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private static ObjectMapper objectMapper = new ObjectMapper();

    public Double getAggregateAmount() {
        return aggregateAmount;
    }

    public void setAggregateAmount(Double aggregateAmount) {
        this.aggregateAmount = aggregateAmount;
    }

    public long getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(long lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public static BankBalanceAggregate empty() {
        BankBalanceAggregate bankBalanceAggregate = new BankBalanceAggregate();
        bankBalanceAggregate.setAggregateAmount(0d);
        bankBalanceAggregate.setLastUpdatedAt(0l);
        return bankBalanceAggregate;
//        try {
//            return objectMapper.writeValueAsString(bankBalanceAggregate);
//        } catch (JsonProcessingException e) {
//            e.printStackTrace();
//        }
//        return null;
    }

    public BankBalanceAggregate() {
    }

    public BankBalanceAggregate(Double aggregateAmount, long lastUpdatedAt) {
        this.aggregateAmount = aggregateAmount;
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public static BankBalanceAggregate adder(String key, String value, BankBalanceAggregate oldAggregate) {
        try {
//            BankBalanceAggregate oldAggregate;
//            if(oldAggregateStr != null && !oldAggregateStr.equalsIgnoreCase("")) {
//                oldAggregate = objectMapper.readValue(oldAggregateStr, BankBalanceAggregate.class);
//            }
//            else {
//                oldAggregate = new BankBalanceAggregate(0.0d, Instant.EPOCH);
//            }
            BankBalanceAggregate bankBalanceAggregate = new BankBalanceAggregate(oldAggregate.getAggregateAmount(), oldAggregate.getLastUpdatedAt());
            Data data = objectMapper.readValue(value, Data.class);
            Double prevSum = bankBalanceAggregate.getAggregateAmount();
            bankBalanceAggregate.setAggregateAmount(data.getMoney() + prevSum);
            long oldLastUpdated = bankBalanceAggregate.getLastUpdatedAt();
            if (oldAggregate.getLastUpdatedAt() < data.getUpdatedTime()) {
                bankBalanceAggregate.setLastUpdatedAt(data.getUpdatedTime());
            }
            System.out.println(key + " -> PrevSum:" + prevSum + " newSum:" + bankBalanceAggregate.getAggregateAmount() + "  PrevLastUpdated: " + oldLastUpdated + " NewLastUpdated: " + bankBalanceAggregate.getLastUpdatedAt());
            return bankBalanceAggregate;
//            return objectMapper.writeValueAsString(bankBalanceAggregate);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return oldAggregate;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BankBalanceAggregate)) return false;
        BankBalanceAggregate that = (BankBalanceAggregate) o;
        return getLastUpdatedAt() == that.getLastUpdatedAt() &&
                Objects.equals(getAggregateAmount(), that.getAggregateAmount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAggregateAmount(), getLastUpdatedAt());
    }

    //    public static class jsonSerde extends Serdes.WrapperSerde<BankBalanceAggregate> {
//
//        public jsonSerde(Serializer<BankBalanceAggregate> serializer, Deserializer<BankBalanceAggregate> deserializer) {
//            super(serializer, deserializer);
//        }
//    }

}
