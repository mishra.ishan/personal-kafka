package DynamicTopology;

import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.ValueTransformer;
import org.apache.kafka.streams.kstream.ValueTransformerWithKey;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.Punctuator;
import org.apache.kafka.streams.processor.StateStore;
import org.apache.kafka.streams.state.KeyValueStore;
import org.apache.kafka.streams.state.StoreBuilder;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CustomTransformerValue implements ValueTransformerWithKey<String, String, String> {

    private KeyValueStore<String, String> store;
    private String currentValue;
    private String storeName;

    public CustomTransformerValue(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public void init(ProcessorContext context) {
        store = (KeyValueStore<String, String>) context.getStateStore(storeName);
        context.schedule(Duration.ofSeconds(1), PunctuationType.WALL_CLOCK_TIME, new Punctuator() {
            @Override
            public void punctuate(long timestamp) {
                final long convert = TimeUnit.MINUTES.convert(timestamp, TimeUnit.MILLISECONDS);
                System.out.println("--------------> Within Punctuate Method <-----------------------" + convert);
                final String getVal = store.get(String.valueOf(convert));
                context.forward(String.valueOf(convert), getVal);
            }
        });
    }

    @Override
    public String transform(String readOnlyKey, String value) {
        final long minutesFromEpoch = TimeUnit.MINUTES.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        final String key = String.valueOf(minutesFromEpoch);
        final String current = store.get(key);
        store.putIfAbsent(key, readOnlyKey + " " +value);
        if(current == null || current.length() == 0) {
            return "nopes. Not found. Hence, updating ";
        }
        return "Found. Hence didn't update. The already present value  -> " + current;
    }

//    @Override
//    public List<String> transform(Long readOnlyKey, List<String> value) {
//        if(currentValue.contains("current")) {
//            return Arrays.asList(currentValue);
//        }
//        else {
//            Long key = TimeUnit.MINUTES.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS) + 1;
//            final List<String> stringList = timerStore.get(key);
//            List<String> finalVal;
//            if(stringList != null) {
//                finalVal = new ArrayList<>(stringList);
//            }
//            else {
//                finalVal = new ArrayList<>();
//            }
//            finalVal.addAll(value);
//            timerStore.put(key, finalVal);
//        }
//        return timerStore.get(readOnlyKey);
//    }



    @Override
    public void close() {

    }
}
