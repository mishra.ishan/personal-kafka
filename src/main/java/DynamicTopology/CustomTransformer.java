package DynamicTopology;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.PunctuationType;
import org.apache.kafka.streams.processor.Punctuator;
import org.apache.kafka.streams.state.KeyValueStore;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public class CustomTransformer implements Transformer<String, String, KeyValue<String, String>> {
    private KeyValueStore<String, String> store;
    private String currentValue;
    private String storeName;

    public CustomTransformer(String storeName) {
        this.storeName = storeName;
    }

    @Override
    public void init(ProcessorContext context) {
        store = (KeyValueStore<String, String>) context.getStateStore(storeName);
        context.schedule(Duration.ofSeconds(1), PunctuationType.WALL_CLOCK_TIME, new Punctuator() {
            @Override
            public void punctuate(long timestamp) {
                final long convert = TimeUnit.MINUTES.convert(timestamp, TimeUnit.MILLISECONDS);
//                System.out.println("--------------> Within Punctuate Method <-----------------------" + convert);
                final String getVal = store.get(String.valueOf(convert));
                context.forward(String.valueOf(convert), getVal);
            }
        });
    }

    @Override
    public KeyValue<String, String> transform(String readOnlyKey, String value) {
        final long minutesFromEpoch = TimeUnit.MINUTES.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        final String key = String.valueOf(minutesFromEpoch);
        final String current = store.get(key);
        store.putIfAbsent(key, readOnlyKey + " " +value);
        if(current == null || current.length() == 0) {
            return KeyValue.pair("NotFound", "nopes. Not found. Hence, updating ");
        }
        return KeyValue.pair("Found", "Hence didn't update. The already present value  -> " + current);
    }

    @Override
    public void close() {

    }
}
