package DynamicTopology;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.state.StoreBuilder;
import org.apache.kafka.streams.state.Stores;

import java.io.InputStream;
import java.util.Properties;

public class DynamicTopologyCreation {
    public static void main(String[] args) {
        Properties properties = new Properties();
        StoreBuilder storeBuilder = Stores.keyValueStoreBuilder(Stores.persistentKeyValueStore("TransformerTestStore"), new Serdes.StringSerde(), new Serdes.StringSerde());
        try(InputStream local_kafka = DynamicTopologyCreation.class.getClassLoader().getResourceAsStream("local_kafka.properties")) {
            properties.load(local_kafka);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        System.out.println("---------------------------------------------------------------> " + properties);

        StreamsBuilder streamsBuilder = new StreamsBuilder();
        streamsBuilder.addStateStore(storeBuilder);

        streamsBuilder.<String, String>stream("TransformerTest2")
//                .peek((k,v) -> System.out.println("-----------------------------------------------------------------------------> " + k + " " + v))
                .transform(new CustomTransformerSupplier(storeBuilder.name()), storeBuilder.name())
        .foreach((k,v) -> System.out.println(v));

        final Topology build = streamsBuilder.build();
        String topologyDescription = build.describe().toString();
        System.out.println(topologyDescription);
        KafkaStreams streams = new KafkaStreams(build, properties);
        streams.cleanUp();
        streams.start();

        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }
}
