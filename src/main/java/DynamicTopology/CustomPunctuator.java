package DynamicTopology;

import org.apache.kafka.streams.processor.Punctuator;
import org.apache.kafka.streams.state.KeyValueStore;

import java.util.concurrent.TimeUnit;

public class CustomPunctuator implements Punctuator {

    private final KeyValueStore<String, String> store;

    public CustomPunctuator(KeyValueStore<String, String> store) {
        this.store = store;
    }

    @Override
    public void punctuate(long timestamp) {
        final long convert = TimeUnit.MINUTES.convert(timestamp, TimeUnit.MILLISECONDS);
        System.out.println("--------------> Within Punctuate Method <-----------------------" + convert);
        final String getVal = store.get(String.valueOf(convert));
    }
}
