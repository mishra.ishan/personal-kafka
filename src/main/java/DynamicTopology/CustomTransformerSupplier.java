package DynamicTopology;

import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.kstream.Transformer;
import org.apache.kafka.streams.kstream.TransformerSupplier;
import org.apache.kafka.streams.processor.ProcessorContext;
import org.apache.kafka.streams.processor.StateStore;

public class CustomTransformerSupplier implements TransformerSupplier<String, String, KeyValue<String, String>> {

    private String storeName;

    public CustomTransformerSupplier(String stateStoreName) {
        this.storeName = stateStoreName;
    }

    @Override
    public Transformer<String, String, KeyValue<String, String>> get() {
        return new CustomTransformer(storeName);
    }
}
