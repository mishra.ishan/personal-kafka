import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ExecutionException;

public class DummyProducer {
    private static String[] colours = {"blue", "green", "yellow", "purple", "cyan", "red", "black", "white", "violet"};
    private static String[] names = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J"};

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        List<String> colourArray = new ArrayList<>(Arrays.asList(colours));
        List<String> namesArray = new ArrayList<>(Arrays.asList(names));
        Collections.shuffle(colourArray);
        Collections.shuffle(namesArray);

        int aindex = 0;
        int nindex = 0;

        final Logger logger = LoggerFactory.getLogger(DummyProducer.class);

        String bootstrapServers = "127.0.0.1:9092";

        String topic = "lucky-colour-input-topic";

        // create Producer properties
        Properties properties = new Properties();
        properties.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        properties.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        // create the producer
        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(properties);

        for (int i = 0; i < 100000; ++i) {

            String key = namesArray.get(aindex++);
            String value = colourArray.get(nindex++);

            ProducerRecord<String, String> record =
                    new ProducerRecord<String, String>(topic, key, value);

            System.out.println(key + "," + value);

            producer.send(record, new Callback() {
                public void onCompletion(RecordMetadata recordMetadata, Exception e) {
                    // executes every time a record is successfully sent or an exception is thrown
                    if (e == null) {
                        // the record was successfully sent
                        logger.info("Received new metadata. \n" +
                                "Topic:" + recordMetadata.topic() + "\n" +
                                "Partition: " + recordMetadata.partition() + "\n" +
                                "Offset: " + recordMetadata.offset() + "\n" +
                                "Timestamp: " + recordMetadata.timestamp());
                    } else {
                        logger.error("Error while producing", e);
                    }
                }
            }).get(); // block the .send() to make it synchronous - don't do this in production!

            if (aindex == namesArray.size()) {
                aindex = 0;
                Collections.shuffle(namesArray);
            }
            if (nindex == colourArray.size()) {
                nindex = 0;
                Collections.shuffle(colourArray);
            }
        }
    }
}
